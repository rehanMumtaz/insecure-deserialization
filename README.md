# Vulnerability: Remote Code Execution via Insecure Deserialization

## Purpose
This is the fourth installment in a deliberately vulnerable application series by the name of NUSK Labs, to help the young and upcoming penetration testers to learn and practice the art hacking.

## Pre-Requisites
1. Good understanding of Web Apps and remote code execution is a must.
2. Make sure you have python and flask installed.
3. For Windows, you'll have to download and install **python** as well as **pip** from browser. For Linux, following command would do:
```
sudo apt-get install python
```
Then download FLASK framework with the following command:
```
pip install flask
```
The following libraries are used in this application and can be downloaded with pip (Just like we did for Flask):
```
import yaml
import base64
from werkzeug.utils import secure_filename
from distutils.log import debug 
from fileinput import filename 
from flask import *
import os
import re

```
## Steps for setup

1. Open CMD/Terminal on your desktop.
2. Run the following command: 
```
git clone https://gitlab.com/nusk-labs/ctf/insecure-deserialization.git
```
3. This will clone the Repo.
4. Then we need to start our FLASK App, for that run:
```
cd insecure-deserialization
python3 server.py
```
5. Then start the challenge, for which you'll have to go to the URL 127.0.0.1:8083/home

## Vulnerability difficulty and Exploitation details
1. Difficulty: Medium
2. Exploitation: Provide the payload you use and steps for exploitation.
